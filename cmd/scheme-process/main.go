// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/events"
)

type options struct {
	ListenAddress          string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the scheme-process api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	SchemeServiceAddress   string `long:"scheme-service-address" env:"SCHEME_SERVICE_ADDRESS" default:"http://localhost:80" description:"Scheme service address."`
	SchemeServiceAPIKey    string `long:"scheme-service-api-key" env:"SCHEME_SERVICE_API_KEY" default:"" description:"API key to use when calling the scheme service."`
	AppManagerDiscoveryUrl string `long:"app-manager-discovery-url" env:"APP_MANAGER_DISCOVERY_URL" default:"" description:"Discovery url of the app manager."`
	AppManagerPublicKey    string `long:"app-manager-public-key" env:"APP_MANAGER_PUBLIC_KEY" default:"" description:"Public Key of the app manager."`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	_, err = ec.ParsePublicKeyFromPem([]byte(cliOptions.AppManagerPublicKey))
	if err != nil {
		event := events.SCP_31
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
	}

	schemeRepository := serviceRepositories.NewSchemeClient(cliOptions.SchemeServiceAddress, cliOptions.SchemeServiceAPIKey)
	documentTypeRepository := repositories.NewDocumentTypeStore()

	appManagerRepository := repositories.NewAppManagerStore(cliOptions.AppManagerDiscoveryUrl, cliOptions.AppManagerPublicKey)

	schemeUseCase := usecases.NewSchemeUseCase(logger, schemeRepository, documentTypeRepository, appManagerRepository)

	router := http_infra.NewRouter(schemeUseCase, logger)

	event := events.SCP_1
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAddress", cliOptions.ListenAddress), zap.Reflect("event", event))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.SCP_2
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
