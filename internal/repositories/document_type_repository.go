// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
)

type DocumentTypeRepository interface {
	GetDocumentTypes() []model.DocumentType
	healthcheck.Checker
}
