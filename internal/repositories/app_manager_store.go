// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"errors"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
)

type AppManagerStore struct {
	appManagerList []model.AppManager
}

var ErrAppManagerNotFound = errors.New("app manager does not exist")

func NewAppManagerStore(discoveryUrl string, publicKey string) *AppManagerStore {
	return &AppManagerStore{
		appManagerList: []model.AppManager{
			{
				Oin:          "00000001001172773000",
				Name:         "Gemeente Tilburg",
				DiscoveryUrl: discoveryUrl,
				PublicKey:    publicKey,
			}}}
}

func (s *AppManagerStore) GetAppManagerList() []model.AppManager {
	return s.appManagerList
}

func (s *AppManagerStore) GetAppManager(oin string) (*model.AppManager, error) {
	result := []model.AppManager{}

	for _, s := range s.appManagerList {
		if s.Oin == oin {
			result = append(result, s)
			break
		}
	}

	if len(result) == 0 {
		return nil, ErrAppManagerNotFound
	}

	return &result[0], nil
}

func (s *AppManagerStore) GetHealthCheck() healthcheck.Result {
	return healthcheck.Result{
		Name:         "app-manager-store",
		Status:       healthcheck.StatusOK,
		ResponseTime: 0,
		HealthChecks: []healthcheck.Result{},
	}
}
