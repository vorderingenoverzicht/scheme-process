// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
)

func handlerFetchOrganizationsList(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	schemeUseCase, _ := context.Value(schemeUseCaseKey).(*usecases.SchemeUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SCP_14
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	organizations, err := schemeUseCase.ListOrganizations()
	if err != nil {
		event := events.SCP_20
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*organizations)
	if err != nil {
		event := events.SCP_6
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SCP_15
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerRegisterOrganization(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	schemeUseCase, _ := context.Value(schemeUseCaseKey).(*usecases.SchemeUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SCP_16
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var organization model.Organization
	err := json.NewDecoder(request.Body).Decode(&organization)
	if err != nil {
		event := events.SCP_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	registeredOrganization, err := schemeUseCase.RegisterOrganization(organization)
	if err != nil {
		event := events.SCP_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*registeredOrganization)
	if err != nil {
		event := events.SCP_6
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SCP_17
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerFetchOrganization(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	schemeUseCase, _ := context.Value(schemeUseCaseKey).(*usecases.SchemeUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SCP_18
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	organization, err := schemeUseCase.FetchOrganization(chi.URLParam(request, "oin"))

	if errors.Is(err, serviceRepositories.ErrOrganizationNotFound) {
		event := events.SCP_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "Not Found", http.StatusNotFound)
		return
	}

	if err != nil {
		event := events.SCP_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*organization)
	if err != nil {
		event := events.SCP_6
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SCP_19
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
