// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/http_infra/metrics"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/usecases"
)

type key int

const (
	schemeUseCaseKey key = iota
	loggerKey        key = iota
)

func NewRouter(schemeUseCase *usecases.SchemeUseCase, logger *zap.Logger) *chi.Mux {
	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
	})
	r.Use(cors.Handler)

	metricsMiddleware := metrics.NewMiddleware("scheme-process")
	r.Use(metricsMiddleware)
	r.Handle("/metrics", promhttp.Handler())

	r.Route("/v1", func(r chi.Router) {
		r.Use(middleware.SetHeader("API-Version", "1.0.0"))

		r.Route("/fetch_scheme", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), schemeUseCaseKey, schemeUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerFetchScheme(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/fetch_organizations", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), schemeUseCaseKey, schemeUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerFetchOrganizationsList(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/register_organization", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), schemeUseCaseKey, schemeUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerRegisterOrganization(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/fetch_organization", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Get("/{oin}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), schemeUseCaseKey, schemeUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerFetchOrganization(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/fetch_app_manager", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Get("/{oin}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), schemeUseCaseKey, schemeUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerFetchAppManager(responseWriter, request.WithContext(ctx))
			})
		})

		r.Get("/openapi.json", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerJson(responseWriter, request.WithContext(ctx))
		})

		r.Get("/openapi.yaml", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerYaml(responseWriter, request.WithContext(ctx))
		})

		healthCheckHandler := healthcheck.NewHandler("scheme-process", schemeUseCase.GetHealthChecks())
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})
	})

	return r
}
