stages:
  - lint
  - scan
  - test
  - generate
  - release
  - tag
  - helm-package
  - docker-image

Go linter:
  stage: lint
  image: golangci/golangci-lint:v1.59.1-alpine
  variables:
    GIT_DEPTH: "0"
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - helm/Chart.yaml
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  before_script:
    - export PATH=$PATH:/usr/local/go/bin
  script:
    - golangci-lint version
    - golangci-lint run --timeout=5m --new-from-rev $(git rev-parse origin/master) --out-format code-climate > code-climate.json
  artifacts:
    reports:
      codequality: code-climate.json
  tags:
    - saas-linux-small-amd64

Go tests:
  stage: test
  image: golang:1.22.3-alpine
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - helm/Chart.yaml
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  before_script:
    - mkdir -p tmp-coverage-reports/
  script:
    - /usr/local/go/bin/go mod download
    - /usr/local/go/bin/go test ./... -v -coverpkg=./... -coverprofile tmp-coverage-reports/coverage.out
    - /usr/local/go/bin/go test ./... -v -coverpkg=./... -json > tmp-coverage-reports/report.json
    - /usr/local/go/bin/go tool cover -html=tmp-coverage-reports/coverage.out -o tmp-coverage-reports/coverage.html
    - /usr/local/go/bin/go tool cover -func=tmp-coverage-reports/coverage.out
  coverage: /total:\t+\(statements\)\t+([\d\.]+?%)/
  artifacts:
    expire_in: 1 week
    paths:
      - tmp-coverage-reports
  tags:
    - saas-linux-small-amd64

sast:
  stage: scan
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/Container-Scanning.gitlab-ci.yml
    rules:
      - if: $CI_COMMIT_TAG

sonarcloud-check:
  stage: scan
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - helm/Chart.yaml
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  script:
    - sonar-scanner

Owasp dependency check:
  image: docker:latest
  stage: scan
  services:
    - docker:dind
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - helm/Chart.yaml
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - DC_VERSION="latest"
    - DC_DIRECTORY=$HOME/OWASP-Dependency-Check
    - 'DC_PROJECT="dependency-check scan: $(pwd)"'
    - DATA_DIRECTORY="$DC_DIRECTORY/data"
    - CACHE_DIRECTORY="$DC_DIRECTORY/data/cache"
    - mkdir -p "$DATA_DIRECTORY"
    - mkdir -p "$CACHE_DIRECTORY"
    - docker pull registry.gitlab.com/blauwe-knop/tools/owasp-dependency-scan:$DC_VERSION
    - |
      docker run --rm \
      -e user=$USER \
      -u $(id -u ${USER}):$(id -g ${USER}) \
      --volume $(pwd):/src:z \
      --volume $(pwd)/odc-reports:/report:z \
      registry.gitlab.com/blauwe-knop/tools/owasp-dependency-scan:$DC_VERSION \
      --scan /src \
      --format "ALL" \
      --project "$DC_PROJECT" \
      --out /report \
      --failOnCVSS 1 \
      --enableExperimental \
      --suppression "owasp-suppression.xml"
  artifacts:
    paths:
      - odc-reports/dependency-check-report.html
      - odc-reports/dependency-check-report.json
      - odc-reports/dependency-check-gitlab.json
    expose_as: owasp-report
    when: always
    expire_in: 1 month
  allow_failure: true
  tags:
    - saas-linux-small-amd64

Trivy local repository scan:
  image: docker:latest
  stage: scan
  services:
    - docker:dind
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - helm/Chart.yaml
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  script:
    - VERSION="0.57.0"
    - |
      docker run \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v $HOME/Library/Caches:/root/.cache/ \
      -v $PWD:/myapp \
      aquasec/trivy:$VERSION repo \
      --scanners vuln,misconfig,secret,license \
      --db-repository public.ecr.aws/aquasecurity/trivy-db:2 \
      --ignore-unfixed \
      --exit-code 1 \
      /myapp
  allow_failure:
    exit_codes:
      - 1
  tags:
    - saas-linux-small-amd64

Generate openapi json:
  image: docker:latest
  stage: generate
  services:
    - docker:dind
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - helm/Chart.yaml
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  script:
    - docker run --rm -v "${PWD}/api:/api" openapitools/openapi-generator-cli:v7.9.0 generate -i /api/openapi.yaml -o /api -g openapi
  artifacts:
    paths:
      - api/openapi.json
    expire_in: 1 week
  tags:
    - saas-linux-small-amd64

Create release:
  image: alpine:latest
  stage: release
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - helm/Chart.yaml
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
  before_script:
    - apk add --update --no-cache git
    - git config --global user.email "${BK_CI_JOB_USER}@noreply.gitlab.com"
    - git config --global user.name "BK GitLab CI"
  script:
    - cat helm/Chart.yaml
    - 'export RELEASE_VERSION=$(cat helm/Chart.yaml | sed -n "s/^version: \(.*\)$/\1/p")'
    - NEXT_RELEASE_VERSION=$(echo $RELEASE_VERSION | awk -F. -v OFS=. '{$NF += 1 ; print}')
    - sed -i "s/^\(version:\).*$/\1 $NEXT_RELEASE_VERSION/g" helm/Chart.yaml
    - sed -i "s/^\(appVersion:\).*$/\1 $NEXT_RELEASE_VERSION/g" helm/Chart.yaml
    - cat helm/Chart.yaml
    - git add helm/Chart.yaml
    - git commit -m "Release $NEXT_RELEASE_VERSION"
    - git push "https://${BK_CI_JOB_USER}:${BK_CI_JOB_TOKEN}@gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process.git" HEAD:master

Create tag:
  image: alpine:latest
  stage: tag
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        -  helm/Chart.yaml
  before_script:
    - apk add --update --no-cache git
    - git config --global user.email "${BK_CI_JOB_USER}@noreply.gitlab.com"
    - git config --global user.name "BK GitLab CI"
  script:
    - cat helm/Chart.yaml
    - 'export RELEASE_VERSION=$(cat helm/Chart.yaml | sed -n "s/^version: \(.*\)$/\1/p")'
    - git tag v$RELEASE_VERSION
    - git push "https://${BK_CI_JOB_USER}:${BK_CI_JOB_TOKEN}@gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process.git" tag v$RELEASE_VERSION

Build docker image:
  image: docker:latest
  stage: docker-image
  rules:
    - if: $CI_COMMIT_TAG
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - cat helm/Chart.yaml
    - 'export RELEASE_VERSION=$(cat helm/Chart.yaml | sed -n "s/^version: \(.*\)$/\1/p")'
    - docker build --pull -t "$CI_REGISTRY_IMAGE" -t "$CI_REGISTRY_IMAGE:$RELEASE_VERSION" .
    - docker push "$CI_REGISTRY_IMAGE:$RELEASE_VERSION"
    - docker push "$CI_REGISTRY_IMAGE"
  tags:
    - saas-linux-small-amd64

Helm package:
  image: alpine:latest
  stage: helm-package
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - apk add --update --no-cache wget git curl
    - wget https://get.helm.sh/helm-v3.9.0-linux-amd64.tar.gz -O - | tar -xz
    - mv linux-amd64/helm /usr/bin/helm
    - chmod +x /usr/bin/helm
    - rm -rf linux-amd64
    - git config --global user.email "${BK_CI_JOB_USER}@noreply.gitlab.com"
    - git config --global user.name "BK GitLab CI"
  script:
    - helm package helm --destination "helm-package"
    - git clone https://${BK_CI_JOB_USER}:${BK_CI_JOB_TOKEN}@gitlab.com/blauwe-knop/vorderingenoverzicht/charts.git
    - mv helm-package/* charts/public/repo
    - cd charts
    - git status
    - git add .
    - git commit -m "Add helm package ${CI_PROJECT_NAME} ${CI_COMMIT_TAG}"
    - git push
