// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package schemeprocess_test

import (
	"errors"
	"testing"

	"go.uber.org/mock/gomock"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"
	"gotest.tools/assert"

	servicemodel "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"
	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/test/mock"
)

func TestSchemeUseCase(t *testing.T) {
	t.Run("FetchScheme", func(t *testing.T) {
		type fields struct {
			schemeRepository       serviceRepositories.SchemeRepository
			documentTypeRepository repositories.DocumentTypeRepository
			appManagerRepository   repositories.AppManagerRepository
		}
		type args struct {
		}
		errListOrganizationsFailed := errors.New("ErrListOrganizationsFailed")

		mockOrganizations := &[]servicemodel.Organization{
			{
				Oin:          "oin",
				Name:         "name",
				DiscoveryUrl: "http://mock-url/v1",
				Approved:     true,
			},
		}

		mockDocumentTypes := []model.DocumentType{{Name: "Request"}}
		mockAppManagers := []model.AppManager{
			{
				Oin:          "oin",
				Name:         "name",
				DiscoveryUrl: "http://mock-url/v1",
				PublicKey:    "public-key",
			},
		}

		mockScheme := &model.Scheme{
			Organizations: *mockOrganizations,
			DocumentTypes: mockDocumentTypes,
			AppManagers:   mockAppManagers,
		}

		tests := []struct {
			name           string
			fields         fields
			args           args
			expectedError  error
			expectedScheme *model.Scheme
		}{
			{
				name: "returns an error when no organizations can be returned",
				fields: fields{
					schemeRepository: func() serviceRepositories.SchemeRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockSchemeRepository(ctrl)
						repo.EXPECT().ListOrganizations().Return(nil, errListOrganizationsFailed).AnyTimes()
						return repo
					}(),
				},
				args:           args{},
				expectedError:  errListOrganizationsFailed,
				expectedScheme: nil,
			},
			{
				name: "returns a scheme",
				fields: fields{
					schemeRepository: func() serviceRepositories.SchemeRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockSchemeRepository(ctrl)
						repo.EXPECT().ListOrganizations().Return(mockOrganizations, nil).AnyTimes()
						return repo
					}(),
					documentTypeRepository: func() repositories.DocumentTypeRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockDocumentTypeRepository(ctrl)
						repo.EXPECT().GetDocumentTypes().Return(mockDocumentTypes).AnyTimes()
						return repo
					}(),
					appManagerRepository: func() repositories.AppManagerRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockAppManagerRepository(ctrl)
						repo.EXPECT().GetAppManagerList().Return(mockAppManagers).AnyTimes()
						return repo
					}(),
				},
				args:           args{},
				expectedError:  nil,
				expectedScheme: mockScheme,
			},
		}

		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {

				observedZapCore, _ := observer.New(zap.InfoLevel)
				observedLogger := zap.New(observedZapCore)
				schemeProcessUserCase := usecases.NewSchemeUseCase(
					observedLogger,
					tt.fields.schemeRepository,
					tt.fields.documentTypeRepository,
					tt.fields.appManagerRepository,
				)

				scheme, err := schemeProcessUserCase.FetchScheme()
				assert.DeepEqual(t, tt.expectedScheme, scheme)
				assert.Equal(t, tt.expectedError, err)
			})
		}
	})
}
