#!/bin/bash

mockgen -source=./../internal/repositories/app_manager_repository.go -destination=./../test/mock/app_manager_repository.go -package=mock
mockgen -source=./../internal/repositories/document_type_repository.go -destination=./../test/mock/document_type_repository.go -package=mock
mockgen -destination=./../test/mock/scheme_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/repositories SchemeRepository
