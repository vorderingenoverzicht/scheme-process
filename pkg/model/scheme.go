// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"

type Scheme struct {
	Organizations []model.Organization `json:"organizations"`
	AppManagers   []AppManager         `json:"appManagers"`
	DocumentTypes []DocumentType       `json:"documentTypes"`
}
