// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
)

type SchemeClient struct {
	baseURL string
}

var ErrOrganizationNotFound = errors.New("organization does not exist")
var ErrAppManagerNotFound = errors.New("app manager does not exist")

func NewSchemeClient(baseURL string) *SchemeClient {
	return &SchemeClient{
		baseURL: baseURL,
	}
}

func (s *SchemeClient) FetchAppManager(oin string) (*model.AppManager, error) {
	url := fmt.Sprintf("%s/fetch_app_manager/%s", s.baseURL, oin)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch app manager request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch app manager: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, ErrAppManagerNotFound
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving app managers: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	appManager := model.AppManager{}
	err = json.Unmarshal(body, &appManager)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &appManager, nil
}

func (s *SchemeClient) RegisterOrganization(organization model.Organization) (*serviceModel.Organization, error) {
	url := fmt.Sprintf("%s/register_organization", s.baseURL)

	requestBodyAsJson, err := json.Marshal(organization)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to post organization request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to post organization: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving organizations: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	returnOrganization := serviceModel.Organization{}
	err = json.Unmarshal(body, &returnOrganization)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &returnOrganization, nil
}

func (s *SchemeClient) FetchOrganization(oin string) (*serviceModel.Organization, error) {
	url := fmt.Sprintf("%s/fetch_organization/%s", s.baseURL, oin)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to get organizations request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get organizations: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, ErrOrganizationNotFound
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving organizations: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	organization := serviceModel.Organization{}
	err = json.Unmarshal(body, &organization)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &organization, nil
}
