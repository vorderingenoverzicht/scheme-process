// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
)

type SchemeRepository interface {
	FetchAppManager(oin string) (*model.AppManager, error)
	RegisterOrganization(model.Organization) (*serviceModel.Organization, error)
	FetchOrganization(oin string) (*serviceModel.Organization, error)
}
